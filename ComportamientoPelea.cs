﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoPelea : MonoBehaviour
{
    Arma l1;
    Arma l2;
    Personaje p1;
    Personaje p2;

    void Start()
    {
        l1 = new Arma("katana", 30);
        l2 = new Arma("motosierra", 30);
        p1 = new Personaje("Ryu", 500, 20, 20, l1);
        p2 = new Personaje("Ken", 500, 20, 20, l2);


        Debug.Log(p1.Nombre + "--vida--" + p1.Vida + "--defensa--" + p1.Defensa + "--ataque--" + p1.Ataque + "--arma--" + l1.Nombre);
        Debug.Log(p2.Nombre + "--vida--" + p2.Vida + "--defensa--" + p2.Defensa + "--ataque--" + p2.Ataque + "--arma--" + l2.Nombre);

        Debug.Log("pulse P para que ken ataque o pulse W para que Ryu ataque");


    }

    // Update is called once per frame
    void Update()
    {

        do
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Debug.Log("el jugador apreto la tecla apreto la tecla W");
                p1.pegar(p2);

                // p2.Vida = p2.Vida - (p2.Defensa - p1.Ataque - l1.Capacidaddeataque);
                Debug.Log(p2.Nombre + "----" + p2.Vida);
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.P))
                {
                    Debug.Log("el jugador apreto la tecla apreto la tecla p");
                    p2.pegar(p1);
                    //   p1.Vida = p1.Vida - (p1.Defensa - p2.Ataque - l2.Capacidaddeataque);
                    Debug.Log(p1.Nombre + "----" + p1.Vida);
                }
            }
        } while (p1.Vida > 0 && p2.Vida > 0); // aca claro que se va a ejecutar esta accion mientras los personajes no tengan vida menor a 0

        if (p1.Vida > 0)
        {
            Debug.Log("El ganador es-----" + p1.Nombre);

        }
        else
        {
            Debug.Log("el ganador es ----" + p2.Nombre);
        }
    }
    public class Personaje
    {
        public string Nombre { get; set; }
        public int Vida { get; set; }
        public int Defensa { get; set; }
        public int Ataque { get; set; }
        public Arma Arma { get; set; }
        public Personaje(string nombre, int vida, int defensa, int ataque, Arma arma)
        {
            Nombre = nombre;
            Vida = vida;
            Defensa = defensa;
            Ataque = ataque;
            Arma = arma;

        }

        public void pegar(Personaje unPersonaje)
        {
            unPersonaje.Vida = unPersonaje.Vida - (unPersonaje.Defensa - unPersonaje.Ataque - Arma.Capacidaddeataque);
        }
    }


    public class Arma
    {

        public string Nombre { get; set; }
        public int Capacidaddeataque { get; set; }

        public Arma(string nombre, int capacidaddeataque)
        {
            Nombre = nombre;
            Capacidaddeataque = capacidaddeataque;
        }

    }


}
