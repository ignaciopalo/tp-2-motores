﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ComportamientoTienda : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Tienda tienda = new Tienda();

        tienda.GenerarDiccionario();


        foreach (var item in tienda.Items)
        {
            Debug.Log(string.Format("{0} ---- {1}", item.Key, item.Value.Costo));
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public class Tienda
    {
        public IDictionary<string, ItemTienda> Items { get; set; } = new Dictionary<string, ItemTienda>();

        public void GenerarDiccionario()
        {
            string abecedario = "";

            //generar string con las letras del abecedario
            for (char i = 'a'; i < 'z'; i++)
            {
                abecedario += i;
            }

            System.Random randomNombre = new System.Random();
            System.Random randomCosto = new System.Random();
            string nombre = "";
            int pos;

            //generar 10000 objetos tienda y cargarlos en el diccionario
            for (int i = 0; i < 10000; i++)
            {
                // validar que la clave no exista en el diccionario
                do
                {
                    // generar nombre
                    for (int j = 0; j < 50; j++)
                    {
                        pos = randomNombre.Next(0, 22);
                        nombre += abecedario[pos];
                    }
                }
                while (Items.ContainsKey(nombre));

                // agregar itemTienda al diccionario items
                Items.Add(nombre, new ItemTienda(nombre, randomCosto.Next(500, 50000)));
            }

            // ordenar diccionario por clave
            Items = Items.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
        }
    }
    public class ItemTienda
    {
        public string Nombre { get; set; }
        public int Costo { get; set; }

        public ItemTienda(string nombre, int costo)
        {
            Nombre = nombre;
            Costo = costo;
        }
    }
}

